# RotationForest
[Link to pip](https://pypi.org/project/rotation-random-forest/)

### Description
This is implementation of Rotation Random Forest model from scratch.

### Visuals
Comparison of RandomForest and RotationForest. \
As you can see, RotationForest can make non-orthogonal separations.

![](img/rf.png)![](img/rrf.png)

### Installation
`pip install rotation-random-forest`

### Usage
[Example of usage](example.ipynb)

### Authors and acknowledgment
[**Article**](https://www.researchgate.net/publication/6806976_Rotation_Forest_A_New_Classifier_Ensemble_Method)
