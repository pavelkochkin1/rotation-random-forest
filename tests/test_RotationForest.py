import pytest
from sklearn.datasets import make_classification

from rotation_random_forest.RotationRandomForest import RotationForest
import numpy as np


@pytest.fixture
def dataset():
    X, y = make_classification(
        n_samples=100,
        n_features=20,
        n_classes=2,
        n_informative=4,
        n_redundant=3,
        n_repeated=2,
        random_state=42,
    )
    yield (X, y)


model = RotationForest()


def test_fit(dataset):
    model.fit(*dataset)


def test_predict(dataset):
    prediction = model.predict(dataset[0])
    assert np.sum(dataset[1] == prediction) == 100


def test_score(dataset):
    score = model.score(*dataset)
    assert score == 1
